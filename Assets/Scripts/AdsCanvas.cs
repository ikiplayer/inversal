﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class AdsCanvas : MonoBehaviour {
	
	private string sceneNameString;
	public GameObject ads;
	private UnityAds unityAds;

	// Use this for initialization
	void Start () {

		sceneNameString = SceneManager.GetActiveScene ().name;
		unityAds = ads.GetComponent<UnityAds> ();

	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void retryClick(){
		SceneManager.LoadScene (sceneNameString);
	}
	public void adsClick(){
		unityAds.ShowAd ();
		
	}
}

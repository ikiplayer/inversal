﻿using UnityEngine;
using System.Collections;

public class Rotation : MonoBehaviour {

	public GameObject rotatedObject;
	public float rotationSpeed;
	public float shrinkSpeed = .5f;
	public float targetScale = 0.1f;
	private bool shrinking;
	private Vector3 localScale;

	// Use this for initialization
	void Start () {
		shrinking = true;
		localScale = rotatedObject.transform.localScale;
		rotationSpeed = Random.Range (rotationSpeed + 20, rotationSpeed + 30);
		
	
	}
	
	// Update is called once per frame
	void Update () {

		rotatedObject.transform.Rotate(Vector3.one * rotationSpeed * Time.deltaTime);

		if (shrinking) {
			rotatedObject.transform.localScale = Vector3.Lerp(rotatedObject.transform.localScale,
				new Vector3(targetScale, targetScale, targetScale), Time.deltaTime*shrinkSpeed);
		}
		if (rotatedObject.transform.localScale.x <= targetScale + 0.05) {
			shrinking = false;
		}
		if (!shrinking) {
			rotatedObject.transform.localScale = Vector3.Lerp(rotatedObject.transform.localScale,
				new Vector3(targetScale*5, targetScale*5, targetScale*5), Time.deltaTime*shrinkSpeed);

		}
		if (rotatedObject.transform.localScale.x > localScale.x) {
			shrinking = true;
		}
	
	}
		
}

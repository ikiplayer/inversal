﻿using UnityEngine;
using System.Collections;

public class LockScreenOrientation : MonoBehaviour {

	public GameObject player;
	private Rigidbody2D playerRb;
	public float lagTime = 1f;
	private WaitForSeconds wait;
	private DeviceOrientation lastOrientation;

	// Use this for initialization
	void Start () {
		playerRb = player.GetComponent<Rigidbody2D> ();
		wait = new WaitForSeconds (lagTime);

	}
	
	// Update is called once per frame
	void Update () {
		DeviceOrientation currentOrientation = Input.deviceOrientation;
		//switch (Input.deviceOrientation);
	//	print (Input.deviceOrientation);
		switch (Input.deviceOrientation) {
		// BECAUSE IT RUNS EVERY FRAME
		 	// NEED TO RUN IT THE COROUTINE ONCE AND ONLY ONCE
		case DeviceOrientation.LandscapeLeft:
		//	stopObjectMovement (DeviceOrientation.LandscapeLeft);
			if (currentOrientation != lastOrientation) {
				StartCoroutine (stopObjectMovement (DeviceOrientation.LandscapeLeft));
			}
			lastOrientation = currentOrientation;

			//player.velocity = new Vector2 (0f, -8f);
			break;
		case DeviceOrientation.LandscapeRight:
			if (currentOrientation != lastOrientation) {
				StartCoroutine (stopObjectMovement (DeviceOrientation.LandscapeRight));
			}
			lastOrientation = currentOrientation;
			break;
		case DeviceOrientation.Portrait:
			if (currentOrientation != lastOrientation) {
				StartCoroutine (stopObjectMovement (DeviceOrientation.LandscapeLeft));
			}
			lastOrientation = currentOrientation;
						
		//	player.velocity = new Vector2 (8f, 0f);
			break;
		case DeviceOrientation.PortraitUpsideDown:
			if (currentOrientation != lastOrientation) {
				StartCoroutine (stopObjectMovement (DeviceOrientation.LandscapeLeft));
			}
			lastOrientation = currentOrientation;
			break;
		}
	
	}
//	IEnumerator playerMovement(DeviceOrientation orientation){
//		
//		switch (Input.deviceOrientation) {
//		case orientation.LandscapeLeft:
//			playerRb.gravityScale = 0f;
//			playerRb.velocity = Vector2.zero;
//			player.transform.Rotate (Vector3.down * Time.deltaTime);
//
//			break;
//		case orientation.LandscapeRight:
//			playerRb.gravityScale = 0f;
//			playerRb.velocity = Vector2.zero;
//			player.transform.Rotate (Vector3.up * Time.deltaTime);
//
//			break;
//		case orientation.Portrait:
//			playerRb.gravityScale = 0f;
//			playerRb.velocity = Vector2.zero;
//			player.transform.Rotate (Vector3.left * Time.deltaTime);
//
//			break;
//		case orientation.PortraitUpsideDown:
//			playerRb.gravityScale = 0f;
//			playerRb.velocity = Vector2.zero;
//			player.transform.Rotate (Vector3.right * Time.deltaTime);
//
//			break;
//		}
//	}
	IEnumerator stopObjectMovement(DeviceOrientation orientation){
		playerRb.gravityScale = 0f;
		playerRb.velocity = Vector2.zero;
		player.transform.position = player.transform.position;
		yield return wait;
		switch (orientation) {
		case DeviceOrientation.LandscapeRight:
		//	player.transform.Rotate (Vector2.down);
			Physics2D.gravity = new Vector2 (0f, 2f);
			break;
		case DeviceOrientation.LandscapeLeft:
		//	player.transform.Rotate (Vector2.up);
			Physics2D.gravity = new Vector2 (0f, -2f);
			break;
		case DeviceOrientation.Portrait:
		//	player.transform.Rotate (Vector2.left);
			Physics2D.gravity = new Vector2 (2f, 0f);
			break;
		case DeviceOrientation.PortraitUpsideDown:
		//	player.transform.Rotate (Vector2.right);
			Physics2D.gravity = new Vector2 (-2f, 0f);
			break;
		}
		playerRb.gravityScale = 5f;
		// TODO:
			// FREEZE OBJECT IN MOTION
			// ROTATE OBJECT DEPENDING ON THE ORIENTATION
			// Wait till completed, when turn on gravity

	}

}

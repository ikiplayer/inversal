﻿using UnityEngine;
using System.Collections;

public class WrapAround : MonoBehaviour
{

	private float centerScreenWidthPlusMargin;
	private float centerScreenHeightPlusMargin;
	public GameObject player;


	// Use this for initialization
	void Start ()
	{
		Camera cam = Camera.main;
		centerScreenHeightPlusMargin = (2f * cam.orthographicSize);
		print (centerScreenHeightPlusMargin + "Center Screen");

		centerScreenWidthPlusMargin = (cam.aspect * centerScreenHeightPlusMargin);
		print (centerScreenWidthPlusMargin + "Center Screeen");

		centerScreenHeightPlusMargin = centerScreenHeightPlusMargin / 2 + 1;
		print (centerScreenHeightPlusMargin + "Center Screen");

		centerScreenWidthPlusMargin = centerScreenWidthPlusMargin / 2 + 1;
		print (centerScreenWidthPlusMargin + "Center Screeen");

	
	}
	
	// Update is called once per frame
	void Update ()
	{
		Vector2 playerPosition = player.transform.position;
		print ("Player Position" + playerPosition);
		// If falls down
		// 
		if (playerPosition.y <= -centerScreenHeightPlusMargin) {
			player.transform.position = new Vector3 (player.transform.position.x, centerScreenHeightPlusMargin, player.transform.position.z);
		}

		if (playerPosition.y >= centerScreenHeightPlusMargin) {
			player.transform.position = new Vector3 (player.transform.position.x, -centerScreenHeightPlusMargin, player.transform.position.z); 
		}

		if (playerPosition.x <= -centerScreenWidthPlusMargin) {
			player.transform.position = new Vector3 (centerScreenWidthPlusMargin, player.transform.position.y, player.transform.position.z); 
		}

		if (playerPosition.x >= centerScreenWidthPlusMargin) {
			player.transform.position = new Vector3 (-centerScreenWidthPlusMargin, player.transform.position.y, player.transform.position.z); 
		}
	
	}
}


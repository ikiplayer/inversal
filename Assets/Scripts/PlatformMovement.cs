﻿using UnityEngine;
using System.Collections;

public class PlatformMovement : MonoBehaviour
{
	public GameObject movingPlatformX = null;
	private Rigidbody2D movingPlatformXRB;

	public float movingPlatformVelocity = 2f;
	private Vector2 movingPlatformHorizontalVelocity;

	Vector2 currentSwipe;
	Vector2 firstPressPos;
	Vector2 secondPressPos;


	// Use this for initialization
	void Start ()
	{
		movingPlatformXRB = movingPlatformX.GetComponent<Rigidbody2D> ();
		movingPlatformHorizontalVelocity = new Vector2 (movingPlatformVelocity, 0f);


	}
	
	// Update is called once per frame
	void Update ()
	{
		movingPlatformXRB.velocity = -movingPlatformHorizontalVelocity;
	
	}
	void OnCollisionEnter2D (Collision2D otherCollision){
		print (otherCollision.collider.name);
		
		switch (otherCollision.gameObject.tag) {
		case "Player":
			break;
		case "Floor":
			if (movingPlatformX != null) {
				movingPlatformHorizontalVelocity *= -1;


			}
			break;	
			
		}
	}
	void OnTriggerEnter2D (Collider2D otherCollision){
		switch (otherCollision.gameObject.tag) {
		case "Invisible":
			if (movingPlatformX != null) {
				// IF PLATFORM IS MOVING RIGHT
				movingPlatformHorizontalVelocity *= -1;

			}
			break;
		}
	
	}
	void OnCollisionStay2D (Collision2D otherCollision){
		print (otherCollision.collider.name);

		switch (otherCollision.gameObject.tag) {
		case "Player":
			touchSwipe ();
			keyboardControls ();
			break;

		}
	}
	void touchSwipe(){


		if(Input.touches.Length > 0)
		{
			Touch t = Input.GetTouch(0);
			if(t.phase == TouchPhase.Began)
			{
				//save began touch 2d point
				firstPressPos = new Vector2(t.position.x,t.position.y);
			}
			if(t.phase == TouchPhase.Ended)
			{
				//save ended touch 2d point
				secondPressPos = new Vector2(t.position.x,t.position.y);

				//create vector from the two points
				currentSwipe = new Vector2(secondPressPos.x - firstPressPos.x, secondPressPos.y - firstPressPos.y);

				//normalize the 2d vector
				currentSwipe.Normalize();

				//swipe upwards
				if(currentSwipe.y > 0 && currentSwipe.x > -0.5f && currentSwipe.x < 0.5f)
				{
					Debug.Log("up swipe");
					Physics2D.gravity = new Vector2 (0f, 2f);

				}
				//swipe down
				if(currentSwipe.y < 0 &&  currentSwipe.x > -0.5f &&  currentSwipe.x < 0.5f)
				{
					Debug.Log("down swipe");
					Physics2D.gravity = new Vector2 (0f, -2f);

				}
				//swipe left
				if(currentSwipe.x < 0 &&  currentSwipe.y > -0.5f &&  currentSwipe.y < 0.5f)
				{
					Debug.Log("left swipe");
					Physics2D.gravity = new Vector2 (-2f, 0f);


				}
				//swipe right
				if(currentSwipe.x > 0  && currentSwipe.y > -0.5f &&  currentSwipe.y < 0.5f)
				{
					Debug.Log("right swipe");
					Physics2D.gravity = new Vector2 (2f, 0f);

				}
			}
		}
	}
	void keyboardControls(){

		if(Input.GetKey(KeyCode.RightArrow)){
			Physics2D.gravity = new Vector2 (2f, 0f);
		}
		if (Input.GetKey (KeyCode.LeftArrow)) {
			Physics2D.gravity = new Vector2 (-2f, 0f);

		}
		if (Input.GetKey (KeyCode.UpArrow)) {
			Physics2D.gravity = new Vector2 (0f, 2f);
		}
		if (Input.GetKey (KeyCode.DownArrow)) {
			Physics2D.gravity = new Vector2 (0f, -2f);

		}


	}
}


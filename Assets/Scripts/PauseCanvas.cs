﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class PauseCanvas : MonoBehaviour {

	private string sceneNameString;

	// Use this for initialization
	void Start () {

		sceneNameString = SceneManager.GetActiveScene ().name;

	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void nextClick(int level){
		SceneManager.LoadScene (level.ToString());

	}
	public void retryClick(){
		print (sceneNameString);
		SceneManager.LoadScene (sceneNameString);

	}
	public void quitClick(){
		SceneManager.LoadScene ("MainMenu");
	}
}

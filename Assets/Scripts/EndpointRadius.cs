﻿using UnityEngine;
using System.Collections;

public class EndpointRadius : MonoBehaviour {

	public GameObject player;
	public GameObject endPoint;
	Rigidbody2D playerRb;

	public float pullRadius = 5;
	public float pullForce = 10;
	public GameObject pauseCanvas;
	public GameObject adsCanvas;

	private Vector2 velocity;
	private Vector3 pastCurrentScale;

	private float randomRange;

	public GameObject ads;
	private UnityAds unityAds;

	// Use this for initialization
	void Start () {
		playerRb = player.GetComponent<Rigidbody2D> ();
		pastCurrentScale = player.transform.localScale;
		pauseCanvas.SetActive (false);
		adsCanvas.SetActive (false);

		randomRange = Random.value;

		unityAds = ads.GetComponent<UnityAds> ();
	
	}
	
	// Update is called once per frame
	void Update () {
		velocity = playerRb.velocity;
//		if (player.transform.localScale <= pastCurrentScale/2) {
//			// Enable Canvas
//			Time.timeScale = 0f;
//
//		}
	
	}
	void OnTriggerEnter2D (Collider2D otherCollider){

		print ("Other Collider" + otherCollider.tag);

	//	Time.timeScale = 0.3f;
		if (otherCollider.tag == "Player") {
			player.transform.localScale = Vector3.Lerp (player.transform.localScale, new Vector2 (0.1f, 0.1f), 
				Time.deltaTime * 10f);
			Invoke ("showSecondChanceCanvas", 0.5f);

		}



		
	}
	void OnTriggerStay2D(Collider2D otherCollider) {
		if (otherCollider.tag == "Player") {
			player.transform.localScale = Vector3.Lerp (player.transform.localScale, new Vector2 (0.1f, 0.1f), 
				Time.deltaTime * 15f);
			Physics2D.gravity = new Vector2(0f,0f);
			player.transform.position = Vector2.Lerp (player.transform.position, endPoint.transform.position, Time.fixedDeltaTime * 20f);
			Vector3 forceDirection = endPoint.transform.position - player.transform.position;
			playerRb.AddForce(forceDirection.normalized * pullForce * Time.fixedDeltaTime *20f);
			playerRb.velocity = Vector2.Lerp (velocity, new Vector2 (0f, 0f), Time.fixedDeltaTime);





		}
	}
	void showSecondChanceCanvas(){
		if (randomRange <= 0.3) {
			adsCanvas.SetActive (true);
			// open retry canvas
		} else {
			pauseCanvas.SetActive (true);
			showRandomAds ();
		}
	}
	void showRandomAds(){
		randomRange = Random.value;
		if (randomRange <= 0.4) {
			unityAds.ShowAd ();
			// Run script
		}
		
	}
}

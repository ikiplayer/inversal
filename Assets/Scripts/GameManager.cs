﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
	public float levelStartDelay = 2f;
	public static GameManager instance = null;

	private int level = 1;

	// Use this for initialization
	void Start ()
	{
		if (instance == null) {
			instance = this;

		} else if (instance != this) {
			Destroy (gameObject);
			DontDestroyOnLoad (gameObject);
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}
}


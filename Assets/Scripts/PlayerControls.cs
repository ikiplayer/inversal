﻿using UnityEngine;
using System.Collections;

public class PlayerControls : MonoBehaviour {
	private float centerScreenWidth;
	private float centerScreenHeight;
	private Vector2 touchPosition;
	Vector2 firstPressPos;
	Vector2 secondPressPos;
	Vector2 currentSwipe;

	public Rigidbody2D player;
	public float velocity = 2f;
	private float checkVelocity = 0.1f;

	// Use this for initialization
	void Start () {

		centerScreenWidth = Screen.width / 2;
		centerScreenHeight = Screen.height / 2;
	
	}
	
	// Update is called once per frame
	void Update () {
		//touchInput ();
		keyboardInput ();
		touchSwipe ();

	


	
	}
	void keyboardInput(){

		if (player.velocity.magnitude <= checkVelocity) {

			if(Input.GetKey(KeyCode.RightArrow)){
				Physics2D.gravity = new Vector2 (2f, 0f);
			}
			if (Input.GetKey (KeyCode.LeftArrow)) {
				Physics2D.gravity = new Vector2 (-2f, 0f);

			}
			if (Input.GetKey (KeyCode.UpArrow)) {
				Physics2D.gravity = new Vector2 (0f, 2f);
			}
			if (Input.GetKey (KeyCode.DownArrow)) {
				Physics2D.gravity = new Vector2 (0f, -2f);

			}




		}

	}



	public void touchSwipe()
	{
		if (player.velocity == Vector2.zero) {


			if(Input.touches.Length > 0)
			{
				Touch t = Input.GetTouch(0);
				if(t.phase == TouchPhase.Began)
				{
					//save began touch 2d point
					firstPressPos = new Vector2(t.position.x,t.position.y);
				}
				if(t.phase == TouchPhase.Ended)
				{
					//save ended touch 2d point
					secondPressPos = new Vector2(t.position.x,t.position.y);

					//create vector from the two points
					currentSwipe = new Vector3(secondPressPos.x - firstPressPos.x, secondPressPos.y - firstPressPos.y);

					//normalize the 2d vector
					currentSwipe.Normalize();

					//swipe upwards
					if(currentSwipe.y > 0 && currentSwipe.x > -0.5f && currentSwipe.x < 0.5f)
					{
						Debug.Log("up swipe");
						Physics2D.gravity = new Vector2 (0f, 2f);

					}
					//swipe down
					if(currentSwipe.y < 0 &&  currentSwipe.x > -0.5f &&  currentSwipe.x < 0.5f)
					{
						Debug.Log("down swipe");
						Physics2D.gravity = new Vector2 (0f, -2f);

					}
					//swipe left
					if(currentSwipe.x < 0 &&  currentSwipe.y > -0.5f &&  currentSwipe.y < 0.5f)
					{
						Debug.Log("left swipe");
						Physics2D.gravity = new Vector2 (-2f, 0f);


					}
					//swipe right
					if(currentSwipe.x > 0  && currentSwipe.y > -0.5f &&  currentSwipe.y < 0.5f)
					{
						Debug.Log("right swipe");
						Physics2D.gravity = new Vector2 (2f, 0f);

					}
				}
			}


		}

	}
	// TODO:
		// CHECK WHETHER THE PLAYER IS IN MIDAIR
//	void touchInput(){
//		// TODO:
//		// GET TOUCH INPUT
//		if (Input.touchCount > 0) {
//			Touch touch = Input.GetTouch (0);
//			touchPosition = touch.position;
//		}
		// CHECH SCREEN WIDTH AND OR HEIGHT
		// GET ORIENTATION
//		switch (Input.deviceOrientation) {
//		case DeviceOrientation.LandscapeLeft:
//			controlsLandscapeLeft (touchPosition);
//			break;
//		case DeviceOrientation.LandscapeRight:
//			controlsLandscapeRight(touchPosition);
//			break;
//		case DeviceOrientation.Portrait:
//			controlsPortrait(touchPosition);
//			break;
//		case DeviceOrientation.PortraitUpsideDown:
//			controlsPortaitUpsideDown(touchPosition);
//			break;

	//	}
//	}
//	void controlsLandscapeLeft(Vector2 inputPosition){
//		if (inputPosition.x > centerScreenWidth) {
//			player.velocity = new Vector2 (velocity, 0f);
//		} else {
//			player.velocity = new Vector2 (-velocity, 0f);
//		}
//	}
//	void controlsLandscapeRight(Vector2 inputPosition){
//		if (inputPosition.x < centerScreenWidth) {
//			player.velocity = new Vector2 (velocity, 0f);
//		} else {
//			player.velocity = new Vector2 (-velocity, 0f);
//		}
//		
//	}
//	void controlsPortrait(Vector2 inputPosition){
//		if (inputPosition.y > centerScreenHeight) {
//			player.velocity = new Vector2 (0, velocity);
//		} else {
//			player.velocity = new Vector2 (0, -velocity);
//		}
//		
//	}
//	void controlsPortaitUpsideDown(Vector2 inputPosition){
//		if (inputPosition.y < centerScreenHeight) {
//			player.velocity = new Vector2 (0, velocity);
//		} else {
//			player.velocity = new Vector2 (0, -velocity);
//		}
//	}
}
